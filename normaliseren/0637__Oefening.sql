-- creer een nieuwe tabel met de auteursgegevens
-- Voornaam en Familienaam
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- gegevens uit de tabel Boeken overzetten naar de tabel Personen
-- we gebruiken hiervoor een subquery
insert into Personen (Voornaam, Familienaam)
select distinct Voornaam, Familienaam 
from Boeken;


-- extra kolommen toevoegen aan tabel Personen
alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null
);

-- relatie tussen tabel Boeken en Personen leggen
-- foreign key moet "not null" zijn maar dat is hier nog niet mogelijk
alter table Boeken add Personen_Id int null;

-- foreign key waardes doorgeven via de primary keys van tabel Personen
-- als de voornaam en achternaam gelijk zijn krijgt het die Id

update Boeken 
cross join Personen
set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and Boeken.Familienaam = Personen.Familienaam;

-- foreign key verplicht maken
alter table Boeken change Personen_Id Personen_Id int not null;

-- overbodige kolommen uit Boeken verwijderen
alter table Boeken 
drop column Voornaam,
drop column Familienaam;

-- constraint van de foreign key toevoegen
alter table Boeken 
add constraint fk_Boeken_Personen foreign key(Personen_Id) references Personen(Id);