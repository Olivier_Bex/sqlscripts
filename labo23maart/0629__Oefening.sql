SELECT studenten.Id
FROM studenten
INNER JOIN evaluaties ON studenten.Id = evaluaties.Studenten_Id
GROUP BY studenten.Id
HAVING AVG(evaluaties.Cijfer) > ( SELECT AVG(Cijfer)
FROM evaluaties)