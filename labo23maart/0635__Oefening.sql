SELECT Leeftijdscategorie, AVG(Loon)
FROM (SELECT Id, Voornaam, floor(Leeftijd / 10) * 10 AS Leeftijdscategorie
FROM personeelsleden) AS Leeftijdscategoriën
INNER JOIN personeelsleden ON Leeftijdscategoriën.Id = personeelsleden.Id
GROUP BY Leeftijdscategorie
ORDER BY Leeftijdscategorie